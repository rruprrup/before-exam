function debounce(func, time) {
  let iTimeout = null
  return (...args) => {
    if (iTimeout) {
      window.clearTimeout(iTimeout)
    }
    iTimeout = window.setTimeout(() => {
      func(...args)
      iTimeout = null
    }, time)
  }
}

class ComboBox {
  constructor(type, placeholder, getItems) {
    this.type = type;
    this.placeholder = placeholder;
    this.getItems = getItems;

    document.querySelector(`#row_${this.type}`).innerHTML += this.render();
    this.hideElement();
    document.querySelector(`#row_${this.type}`).querySelector("input").addEventListener("focus", () => {
      this.showElement();
      this.loadItems();
    });

    document.querySelector(`#row_${this.type}`).querySelector("input").addEventListener("blur", () => {
      this.hideElement();
    });

    document.querySelector(`#row_${this.type}`).querySelector("input").addEventListener("input", () => {
      let inputValue = document.querySelector(`#row_${this.type}`).querySelector("input").value;
      this.loadItems(inputValue);
    });
  }

  render() {
    return `
    <div class="combobox">
      <input name="city" placeholder="${this.placeholder}" value="" autocomplete="off"/>
        <div class="list">
        <div class="items-wrapper">
          <div class="item disabled">Не найдено</div>
         </div>
        </div>
    </div>`;
  }
  showElement() {
    document.querySelector(`#row_${this.type}`).querySelector(".items-wrapper").style.display = "";
  }
  hideElement() {
    document.querySelector(`#row_${this.type}`).querySelector(".items-wrapper").style.display = "none";
  }

  async loadItems(inputValue) {
    // 
    document.querySelector(`#row_${this.type}`).querySelector(".items-wrapper").innerHTML = ` <div class="item disabled">Загрузка</div>`;
    let items = await this.getItems(inputValue);
    //
    document.querySelector(`#row_${this.type}`).querySelector(".items-wrapper").innerHTML = "";
    items.forEach(item => {
      let inputDiv = document.createElement("div");
      inputDiv.classList.add("item");
      inputDiv.textContent = `${item.name}`;
      document.querySelector(`#row_${this.type}`).querySelector(".items-wrapper").appendChild(inputDiv);
    });
  }

}

export { ComboBox };
