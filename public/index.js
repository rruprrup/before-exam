import { ComboBox } from './combobox.js'

/* Реализуйте функцю поиска городов */
async function getCityList(inputValue) {
  let options = {
    method: "GET",
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  };
  let response = await fetch(inputValue ? `/city?name_like=${inputValue}` : `/city`, options);
  if (response.status === 200) {
    return response.json();
  } else {
    throw new Error(`${response.status} ${response.statusText}`)
  }
}
// inputValue
// "?name_like", например: fetch('/city?name_like=Нов')

/* Реализуйте функцю поиска IDE */
async function getIdeList(inputValue) {
  let options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    }
  };
  let response = await fetch(inputValue ? `/ide?name_like=${inputValue}` : `/ide`, options);
  if (response.status === 200) {
    return response.json();
  } else {
    throw new Error(`${response.status} ${response.statusText}`);
  }
}

const city = new ComboBox('city', 'Введите или выберите из списка', getCityList);

const ide = new ComboBox('ide', 'IDE', getIdeList);



/* Добавьте ComboBox для строки "Город"
* например так:
* const city = new ComboBox('city', 'Введите или выберите из списка', getCityList)
* document.getElementById('row_city').appendChild(city.render())
* Ваша реализация может сильно отличаться
* */
/* Добавьте ComboBox для строки "Предпочитаемая IDE"*/